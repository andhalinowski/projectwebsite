Rails.application.routes.draw do
  
  get 'users/new'

  get 'orderitems/index'

  get 'orderitems/shpw'

  get 'orderitems/new'

  get 'orderitems/edit'
  

  devise_for :users do
    resources :orders
  end
  
  resources :orders do
    resources :orderitems
  end
  
  get "cart/index"

  get "site/about"
  get "site/contact"
  get "site/home"
  get "site/portfolio"
  get "site/newborn_gallery"
  get "site/children_gallery"
  get "site/maternity_gallery"
  

  resources :items
  
  get '/cart' => 'cart#index'
  get '/cart/:id' => 'cart#add'
  get '/cart/remove/:id' => 'cart#remove'
  get '/clearCart' => 'cart#clearCart'
  get '/checkout' => 'cart#createOrder'
  
  get '/about' => 'site#about'
  get '/contact' => 'site#contact'
  get '/home' => 'site#home'
  get '/portfolio' => 'site#portfolio'
  get '/newborn_gallery' => 'site#newborn_gallery'
  get '/children_gallery' => 'site#children_gallery'
  get '/maternity_gallery' => 'site#maternity_gallery'

  get '/signup' => 'users#new'
  resources :users
  
  root :to => 'site#home'

 
end


