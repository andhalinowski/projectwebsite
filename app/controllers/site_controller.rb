class SiteController < ApplicationController
  def about
  end

  def contact
  end

  def home
  end

  def portfolio
  end
end
